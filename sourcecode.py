def find_dead_end(location, lst) :
  check = 0
  count = 0
  for i in lst :
    if location in i :
      count = count + 1
  for i in lst :
    #print(i)
    if location in i:
      location_index = i.index(location)
      if count == 1 and i not in dead_ends:
        if location_index == 0 :
          dead_ends.append(i)
        elif location_index == 1 :
          set = i[0]
          max = 0
          for i in lst :
            if set in i:
              max = max + 1  
          if max < 3:
            dead_ends.append([i[1], i[0]])        
      elif i not in dead_ends :
        check= traverse(location, location, i, lst)
        if check and location_index == 0 :
           dead_ends.append(i) 
        elif check and location_index == 1 :
          dead_ends.append([i[1],i[0]]) 

def traverse(fix, location, l1, lst) :
  if l1.index(location) == 0 :
    other_location = l1[1]
  else :
    other_location = l1[0] 
  for i in lst :
    if other_location in i and i != l1 :
      other_location_index = i.index(other_location)
      if other_location_index == 0 :
        key = i[1]
      else :
        key = i[0]
      if key == fix :
         return False
      else :
       c = traverse(fix, other_location,  i, lst)   
       if c == 0 :
         return False
       else:
         return True     
  return True  

def filter_unwanted(location, dead_ends) :
  count = 0
  for i in dead_ends:
    if location in i:
      count = count + 1
  for i in dead_ends:
    if location in i :
      if i.index(location) == 1 :
        delete_unwanted(location, count, dead_ends)

def delete_unwanted(location, count, dead_ends) :
  for i in range(1, count) :
    for i in dead_ends :
      if location in i :
        if i.index(location) == 0 :
          dead_ends.remove(i)

n = 6
m = 5
lst = [[1, 2], [1, 3], [2, 3], [3,4], [1,5], [1,6], [6,7], [6,8]]
dead_ends = [ ]
for location in range(1, n+1) :
  find_dead_end(location, lst)
  filter_unwanted(location, dead_ends)
print(len(dead_ends))
for i in dead_ends:
  print(i[0], end = ' ')
  print(i[1])
